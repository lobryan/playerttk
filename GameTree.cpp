#include "GameTree.h"

void Node::compute_children() {
    if (this->nolookup) {
        return;
    }
    this->isterminal = false;
    std::vector<Node*> result;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (this->board.checkMove(i, j, this->side)) {
                Node *newnode = new Node();
                newnode->board = this->board;
                newnode->board.doMove(i, j, this->side);
                newnode->side = (this->side == WHITE) ? BLACK : WHITE;
                newnode->parent = this;
                newnode->move[0] = i;
                newnode->move[1] = j;
                newnode->isterminal = true;
                result.push_back(newnode);
            }
        }
    }
    // If there were no moves, then the next move has the same board, but a different side
    if (result.size() == 0) {
        Node *newnode = new Node();
        newnode->board = this->board;
        newnode->side = (this->side == WHITE) ? BLACK : WHITE;
        newnode->parent = this;
        newnode->isterminal = true;
        result.push_back(newnode);
    }
    this->children = result;
}

void Node::cut() {
    this->parent->children.erase(std::remove(this->parent->children.begin(), 
                                             this->parent->children.end(), this), 
                                             this->parent->children.end());
    delete this;
}
