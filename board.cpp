#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (checkMove(i, j, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(int u, int v, Side side) {
    // Make sure the square hasn't already been taken.
    if (occupied(u, v)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = u + dx;
            int y = v + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(int u, int v, Side side) {
    // Ignore if move is invalid.
    if (!checkMove(u, v, side)) return;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = u;
            int y = v;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = u;
                y = v;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, u, v);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * A higher score is better here
 */
int Board::eval(Side side) {
    if (side == BLACK) {
        int val = 0;
        
        // Add corner values to the sum
        val += 1000 * (get(BLACK, 0, 0)  + get(BLACK, 0, 7) + get(BLACK, 7, 0) + get(BLACK, 7, 7));
        
        // Add mobility, "number of stones", and edge values to the sum
        // mobility counts for 100
        // each stone counts for 1
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (this->taken[i + 8*j] && this->black[i + 8*j]) {
                    val += 1;
                }
                else if (this->checkMove(i, j, BLACK)) {
                    val += 100;
                }
            }
        }
        
        return val;
    }
    else {
        int val = 0;
        
        // Add corner values to the sum
        val += 1000 * (get(WHITE, 0, 0)  + get(WHITE, 0, 7) + get(WHITE, 7, 0) + get(WHITE, 7, 7));
        
        // Add mobility and "number of stones" values to the sum
        // mobility counts for 100
        // each stone counts for 1
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (this->taken[i + 8*j] && !this->black[i + 8*j]) {
                    val += 1;
                }
                else if (this->checkMove(i, j, WHITE)) {
                    val += 100;
                }
            }
        }
        
        return val;
    }
}

/* transposition table key */
char* Board::key() {
    char *result = new char[64];
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (this->black[i + 8*j]) {
                result[i + 8*j] = 'b';
            }
            else if (this->taken[i + 8*j]) {
                result[i + 8*j] = 'w';
            }
            else {
                result[i + 8*j] = 'u';
            }
        }
    }
    return result;
}



