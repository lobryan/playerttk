The improvements I made, in the order that I tried them, were

* searching the game tree to a certain depth
* alpha-beta pruning
* iterative deepening
* using all the available time
* transposition table

I couldn't get the transposition table to work though, so I left that out (there
was no speedup for me -- maybe it was because I used std::string as the hash key
:/ ).

I had issues getting the parity of the alpha-beta pruning to work out correctly.
I still don't know if I have fixed it completely. What I mean is that if I 
start the search at "depth 0" for the root node each time, sometimes it will
produce the worst move instead of the best move. It seems to be fixed right now,
maybe in a hacky way though.

Also, I changed the evaluation function for terminal nodes. Now it incorporates
mobility, and that seems to work better.

--- Why will my strategies work? ---
Honestly, I don't think they will be better than anyone else's. I think I used
most of the same techniques from the assignment's description.
