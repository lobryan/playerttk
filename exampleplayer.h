#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <stdio.h>
#include "common.h"
#include "board.h"
#include <vector>
#include <queue>
#include <map>
#include "GameTree.h"
#include <time.h>
using namespace std;

class myclass {
    friend class ExamplePlayer;
    struct mycomparison {
        bool operator() (Node *n1, Node *n2) {
            return n1->score < n2->score;
        }
    };
};

class ExamplePlayer {
private:
    Board board;
    Side side;

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
