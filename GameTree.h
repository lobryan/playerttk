#ifndef __GAMETREE_H_
#define __GAMETREE_H_

#include "board.h"
#include <vector>
#include <algorithm>

struct Node {
    int score;
    bool isterminal, nolookup;
    Board board;
    int move[2];
    Side side;
    std::vector<Node*> children;
    Node *parent;
    
    void compute_children();
    void cut();
    Node() {
        this->isterminal = false;
        this->nolookup = false;
    }
    ~Node() {
        std::vector<Node*>::iterator it;
        for (it = this->children.begin(); it != this->children.end(); it++) {
            delete *it;
        }
    }
};

#endif
