CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb
OBJS        = exampleplayer.o wrapper.o board.o gametree.o
PLAYERNAME  = playerttk

all: $(PLAYERNAME) testgame
	
$(PLAYERNAME): $(OBJS)
	$(CC) -o $@ $^
        
TestGame: testgame.o
	$(CC) -o $@ $^
        
%.o: %.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@

gametree.o: GameTree.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@

java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f *.o $(PLAYERNAME) testgame		
	
.PHONY: java

debug.o: debug.cpp
	$(CC) -c $(CFLAGS) debug.cpp

debug: debug.o gametree.o exampleplayer.o board.o
	$(CC) $(CFLAGS) debug.o gametree.o exampleplayer.o board.o -o debug

prune_debug.o: prune_debug.cpp
	$(CC) -c $(CFLAGS) prune_debug.cpp

prune: prune_debug.o gametree.o board.o
	$(CC) $(CFLAGS) prune_debug.o gametree.o board.o -o prune

