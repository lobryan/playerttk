#include "exampleplayer.h"

//int get_score(Node *n, int );
int prune(Node *n, int , int , int, clock_t *begin, float maxtime );
std::vector<Node*> leaves(Node *n);
void deepen(Node *n, int depth);

bool not_finished_prune = false;
Side myside;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    this->side = side;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    // Make opponent's move
    myside = this->side;
    Side otherside = (this->side == WHITE) ? BLACK : WHITE;
    if (opponentsMove != NULL) {
        this->board.doMove(opponentsMove->x, opponentsMove->y, otherside);
    }
    if (!this->board.hasMoves(this->side)) {
        return NULL;
    }
    
    /* maximum number of seconds to use for this move... */
    float maxtime = float(msLeft) / (1000.0 * (64 - this->board.countWhite() - this->board.countBlack())) - .02;
    if (this->board.countWhite() + this->board.countBlack() + 5 > 64) maxtime = 3;
    //fprintf(stderr, "opponent's score: %d\n", this->board.eval(otherside));
    //fprintf(stderr, "my score:         %d\n", this->board.eval(this->side)); 
    not_finished_prune = false;
    
    Node *n = new Node();
    n->parent = NULL;
    n->board = this->board;
    n->side = this->side;
    
    typedef std::priority_queue<Node*, vector<Node*>, myclass::mycomparison> myqueue;
    myqueue queue, copy;
    queue.push(n);
    int score = 0;
    int depth = 0;
    score++;
    std::vector<Node*> myleaves;
    std::vector<Node*>::iterator it, it2, it3;
    clock_t begin = clock();
    Node *best;
    while (1) {
        while (!queue.empty() && float(clock() - begin)/CLOCKS_PER_SEC <= maxtime) {
            Node *it = queue.top();
            queue.pop();
            it->compute_children();
        }
        depth++;
        score = prune(n, depth, 0, 10000, &begin, maxtime);
        if (!not_finished_prune) {
            for (it = n->children.begin(); it != n->children.end(); it++) {
                if ((*it)->score == score) {
                    best = *it;
                    break;
                }
            }
        }
        if (float(clock() - begin)/CLOCKS_PER_SEC >= maxtime) {
            //fprintf(stderr, "time taken: %f\n", float(clock() - begin)/CLOCKS_PER_SEC);
            //fprintf(stderr, "maxdepth  : %d\n", depth);
            Move *move = new Move(best->move[0], best->move[1]);
            this->board.doMove(best->move[0], best->move[1], this->side);
            delete n;
            return move;
            break;
        }
        myleaves = leaves(n);
        for (it = myleaves.begin(); it != myleaves.end() && float(clock() - begin)/CLOCKS_PER_SEC <= maxtime; it++) {
            copy.push(*it);
        }
        queue = copy;
        copy = myqueue();
    }
    return NULL;
    /*** Old Code ***/
    
    /*
    
    std::vector<Node*> positions, copy;
    positions.push_back(n);
    std::vector<Node*>::iterator it, it2;
    
    int depth = (64 < this->board.countWhite() + this->board.countBlack() + 11) ? 11 : DEPTH;
    
    for (int i = 0; i < depth; i++) {
        for (it = positions.begin(); it != positions.end(); it++) {
            std::vector<Node*> all_children = (*it)->compute_children();
            for (it2 = all_children.begin(); it2 != all_children.end(); it2++) {
                char *key = (*it2)->board.key();
                std::string str(key);
                copy.push_back(*it2);
                (*it)->children.push_back(*it2);
            }
        }
        positions = copy;
        copy.clear();
    }
    
    // Now find the correct move (and compute the scores of the leaves)
    int score = get_score(n, 0);
    Node *best;
    for (it = n->children.begin(); it != n->children.end(); it++) {
        if ((*it)->score == score) {
            best = *it;
        }
    } 
    
    Move *move = new Move(best->move[0], best->move[1]);
    this->board.doMove(best->move[0], best->move[1], this->side);
    delete n;
    return move;*/
}

void deepen(Node *n, int depth) {
    if (depth == 0) return;
    std::vector<Node*>::iterator it;
    n->compute_children();
    for (it = n->children.begin(); it != n->children.end(); it++) {
        deepen(*it, depth - 1);
    }
}

std::vector<Node*> leaves(Node *n) {
    std::vector<Node*> results;
    if (n->isterminal) {
        results.push_back(n);
        return results;
    }
    std::vector<Node*>::iterator it;
    for (it = n->children.begin(); it != n->children.end(); it++) {
        std::vector<Node*> tmp_res = leaves(*it);
        results.insert(results.begin(), tmp_res.begin(), tmp_res.end());
    }
    return results;
}

/* Alpha beta prune starting at this node */
int prune(Node *n, int depth, int alpha, int beta, clock_t *begin, float maxtime) {
    if (float(clock() - *begin)/CLOCKS_PER_SEC > maxtime) {
        not_finished_prune = true;
        return 0;
    }
    if (n->isterminal) {
        n->score = n->board.eval(n->side);
        return n->score;
    }
    int score, new_ab;
    std::vector<Node*>::iterator it;
    if ((depth % 2) == 0) {
        new_ab = 0; /* I don't know what I am doing wrong here, but adding
        this line makes the pruning work... */
        for (it = n->children.begin(); it != n->children.end(); it++) {
            score = prune(*it, depth + 1, new_ab, beta, begin, maxtime);
            if (score > new_ab) {
                new_ab = score;
            }
            if (new_ab >= beta) {
                break;
            }
        }
        n->score = new_ab;
        return new_ab;
    }
    else {
        new_ab = 10000; /* and similarly here... */
        for (it = n->children.begin(); it != n->children.end(); it++) {
            score = prune(*it, depth + 1, alpha, new_ab, begin, maxtime);
            if (score < new_ab) {
                new_ab = score;
            }
            if (alpha >= new_ab) {
                break;
            }
        }
        n->score = new_ab;
        return new_ab;
    }
}

/*** Old Code (get_score) ***/

/* Gets the score for this node at this depth. Depth matters because of player order. */
/*int get_score(Node *n, int depth) {
    if (n->children.size() == 0) {
        n->score = n->board.eval(n->side);
        return n->score;
    }
    int score = 0;
    
    if (depth % 2 == DEPTH % 2) {
        int val = 0;
        std::vector<Node*>::iterator it;
        for (it = n->children.begin(); it != n->children.end(); it++) {
            score = get_score(*it, depth + 1);
            if (score > val) {
                val = score;
            }
        }
        n->score = val;
        return val;
    }
    
    else {
        int val = 100000;
        std::vector<Node*>::iterator it;
        for (it = n->children.begin(); it != n->children.end(); it++) {
            score = get_score(*it, depth + 1);
            if (score < val) {
                val = score;
            }
        }
        n->score = val;
        return val;
    }
}*/










